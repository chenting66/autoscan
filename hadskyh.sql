/*
 Navicat Premium Data Transfer

 Source Server         : 172.23.23.123
 Source Server Type    : MySQL
 Source Server Version : 50651
 Source Host           : 172.23.23.123:3309
 Source Schema         : hadskyh

 Target Server Type    : MySQL
 Target Server Version : 50651
 File Encoding         : 65001

 Date: 18/04/2021 15:49:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bbs_cate
-- ----------------------------
DROP TABLE IF EXISTS `bbs_cate`;
CREATE TABLE `bbs_cate`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `cname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '默认板块',
  `uid` int(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_cate
-- ----------------------------
INSERT INTO `bbs_cate` VALUES (5, 41, 'SQL注入', 0);
INSERT INTO `bbs_cate` VALUES (6, 41, 'XSS跨站', 0);
INSERT INTO `bbs_cate` VALUES (7, 41, '命令执行', 0);
INSERT INTO `bbs_cate` VALUES (8, 41, '代码注入', 0);
INSERT INTO `bbs_cate` VALUES (9, 42, '密码找回', 0);
INSERT INTO `bbs_cate` VALUES (10, 42, '越权访问', 0);
INSERT INTO `bbs_cate` VALUES (11, 42, '支付漏洞', 0);
INSERT INTO `bbs_cate` VALUES (12, 42, '隐私泄露', 0);

-- ----------------------------
-- Table structure for bbs_fil
-- ----------------------------
DROP TABLE IF EXISTS `bbs_fil`;
CREATE TABLE `bbs_fil`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hinge` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_fil
-- ----------------------------

-- ----------------------------
-- Table structure for bbs_fri
-- ----------------------------
DROP TABLE IF EXISTS `bbs_fri`;
CREATE TABLE `bbs_fri`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '百度',
  `desc1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '百度一下,你就知道',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'http://www.baidu.com',
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'default_fri.gif',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_fri
-- ----------------------------

-- ----------------------------
-- Table structure for bbs_home_follow
-- ----------------------------
DROP TABLE IF EXISTS `bbs_home_follow`;
CREATE TABLE `bbs_home_follow`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户id',
  `username` char(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '用户名',
  `followuid` int(10) NOT NULL DEFAULT 0 COMMENT '被关注用户ID',
  `fusername` char(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '被关注用户名称',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0:正常 1:特殊关注 -1:不能再关注此人',
  `mutual` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0:单向 1:已互相关注',
  `uptiem` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 32 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of bbs_home_follow
-- ----------------------------
INSERT INTO `bbs_home_follow` VALUES (6, 15, '', 14, '666666', 0, 0, 1526707752);
INSERT INTO `bbs_home_follow` VALUES (5, 17, '', 6, 'admin', 0, 0, 1526685269);
INSERT INTO `bbs_home_follow` VALUES (7, 6, '', 14, '666666', 0, 0, 1526708006);
INSERT INTO `bbs_home_follow` VALUES (8, 12, '', 14, '666666', 0, 0, 1526708011);
INSERT INTO `bbs_home_follow` VALUES (9, 17, '', 14, '666666', 0, 0, 1526708208);
INSERT INTO `bbs_home_follow` VALUES (10, 12, '', 10, '444444', 0, 1, 1526708270);
INSERT INTO `bbs_home_follow` VALUES (11, 6, '', 11, '555555', 0, 0, 1526708306);
INSERT INTO `bbs_home_follow` VALUES (12, 6, '', 8, '222222', 0, 0, 1526708311);
INSERT INTO `bbs_home_follow` VALUES (13, 6, '', 10, '444444', 0, 1, 1526708338);
INSERT INTO `bbs_home_follow` VALUES (14, 9, '', 14, '666666', 0, 0, 1526708773);
INSERT INTO `bbs_home_follow` VALUES (15, 10, '', 14, '666666', 0, 1, 1526708773);
INSERT INTO `bbs_home_follow` VALUES (16, 10, '', 9, '333333', 0, 1, 1526708786);
INSERT INTO `bbs_home_follow` VALUES (17, 9, '', 6, 'admin', 0, 0, 1526708794);
INSERT INTO `bbs_home_follow` VALUES (18, 9, '', 8, '222222', 0, 0, 1526708821);
INSERT INTO `bbs_home_follow` VALUES (19, 10, '', 8, '222222', 0, 0, 1526708821);
INSERT INTO `bbs_home_follow` VALUES (20, 9, '', 12, '777777', 0, 1, 1526708830);
INSERT INTO `bbs_home_follow` VALUES (21, 11, '', 14, '666666', 0, 0, 1526709314);
INSERT INTO `bbs_home_follow` VALUES (22, 10, '', 11, '555555', 0, 0, 1526709330);
INSERT INTO `bbs_home_follow` VALUES (23, 8, '', 11, '555555', 0, 0, 1526709447);
INSERT INTO `bbs_home_follow` VALUES (24, 10, '', 13, '888888', 0, 0, 1526709486);
INSERT INTO `bbs_home_follow` VALUES (25, 8, '', 12, '777777', 0, 1, 1526709868);
INSERT INTO `bbs_home_follow` VALUES (26, 12, '', 6, 'admin', 0, 0, 1526709944);
INSERT INTO `bbs_home_follow` VALUES (27, 10, '', 15, '999999', 0, 0, 1526709954);
INSERT INTO `bbs_home_follow` VALUES (28, 12, '', 15, '999999', 0, 0, 1526709954);
INSERT INTO `bbs_home_follow` VALUES (29, 12, '', 13, '888888', 0, 0, 1526710241);
INSERT INTO `bbs_home_follow` VALUES (30, 12, '', 11, '555555', 0, 0, 1526710434);
INSERT INTO `bbs_home_follow` VALUES (31, 9, '', 13, '888888', 0, 0, 1526710518);

-- ----------------------------
-- Table structure for bbs_iprefuse
-- ----------------------------
DROP TABLE IF EXISTS `bbs_iprefuse`;
CREATE TABLE `bbs_iprefuse`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ipmin` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ipmax` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_iprefuse
-- ----------------------------

-- ----------------------------
-- Table structure for bbs_part
-- ----------------------------
DROP TABLE IF EXISTS `bbs_part`;
CREATE TABLE `bbs_part`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '默认分区',
  `padmins` int(10) NOT NULL DEFAULT 6,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_part
-- ----------------------------
INSERT INTO `bbs_part` VALUES (41, '常规漏洞', 6);
INSERT INTO `bbs_part` VALUES (42, '逻辑漏洞', 6);

-- ----------------------------
-- Table structure for bbs_post
-- ----------------------------
DROP TABLE IF EXISTS `bbs_post`;
CREATE TABLE `bbs_post`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '帖子标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ptime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `pip` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `del` int(1) UNSIGNED NOT NULL DEFAULT 1,
  `view_count` int(11) NOT NULL DEFAULT 0 COMMENT '显示数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 822 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_post
-- ----------------------------
INSERT INTO `bbs_post` VALUES (801, 5, '这是帖子标题', '这是帖子内容', 1528040134, 6, '2130706433', 0, 1, 5);
INSERT INTO `bbs_post` VALUES (816, 6, '111<script >alert(123)</script>', '&lt;p&gt;111&amp;lt;script &amp;gt;alert(123)&amp;lt;/script&amp;gt;&lt;/p&gt;', 1534836270, 6, '2130706433', 0, 1, 1);
INSERT INTO `bbs_post` VALUES (817, 6, '222<script >alert(123)</script>', '&lt;p&gt;222&amp;lt;script &amp;gt;alert(123)&amp;lt;/script&amp;gt;&lt;/p&gt;', 1534837204, 6, '2130706433', 0, 1, 0);
INSERT INTO `bbs_post` VALUES (821, 6, '222<script >alert(123)</script>', '222<script >alert(123)</script>', 1534838162, 6, '2130706433', 0, 1, 1);

-- ----------------------------
-- Table structure for bbs_reply
-- ----------------------------
DROP TABLE IF EXISTS `bbs_reply`;
CREATE TABLE `bbs_reply`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `ptime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `pip` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `xx` int(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 282 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_reply
-- ----------------------------
INSERT INTO `bbs_reply` VALUES (219, 57, '<p><span style=\"color: rgb(97, 97, 97); font-family: -apple-system, system-ui, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, &quot;Malgun Gothic&quot;, &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\">&lt;img src=&quot;http://local.premeate.songboy.net/home/index.php?m=user&amp;a=follow&amp;uid=8&quot;/&gt;</span></p>', 8, 1526709450, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (217, 56, '<p>&lt;img src=&quot;http://local.premeate.songboy.net/home/index.php?m=user&amp;a=follow&amp;uid=8&quot;&gt;</p>', 8, 1526709328, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (218, 57, '<p><span style=\"color: rgb(97, 97, 97); font-family: -apple-system, system-ui, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, &quot;Malgun Gothic&quot;, &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\">&lt;img src=&quot;http://local.premeate.songboy.net/home/index.php?m=user&amp;a=follow&amp;uid=8&quot;/&gt;</span></p>', 8, 1526709437, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (216, 57, '<p>&lt;img src=&quot;http://local.premeate.songboy.net/home/index.php?m=user&amp;a=follow&amp;uid=8&quot;&gt;</p>', 8, 1526709319, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (215, 55, '<p>&lt;img src=&quot;http://local.premeate.songboy.net/home/index.php?m=user&amp;a=follow&amp;uid=8&quot;&gt;</p>', 8, 1526709310, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (180, 55, '<p>1111</p>', 15, 1526622702, 2130706433, 1);
INSERT INTO `bbs_reply` VALUES (214, 56, 'TianYeTest003</p></div><a href=http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=12>', 12, 1526709244, 3232237673, 1);
INSERT INTO `bbs_reply` VALUES (212, 55, '<p>123<br/></p>', 10, 1526709224, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (213, 55, '<p>关注我</p><img src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=10\" />', 10, 1526709242, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (187, 59, '<p>sddddddddddddddddd</p>', 14, 1526708157, 3232237676, 1);
INSERT INTO `bbs_reply` VALUES (188, 55, '<p>sddddddddddddddddd</p>', 14, 1526708170, 3232237676, 1);
INSERT INTO `bbs_reply` VALUES (189, 55, '', 9, 1526708366, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (190, 55, '<script src=\"http://local.premeate.songboy.net/home/index.php?m=user', 9, 1526708454, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (191, 66, '<p>22222222</p>', 8, 1526708471, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (192, 55, 'TianYeTest001</p></div><image src=http://local.premeate.songboy.net/home/index.php?m=user', 12, 1526708486, 3232237673, 1);
INSERT INTO `bbs_reply` VALUES (193, 55, '<p>123<br/></p>', 10, 1526708522, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (194, 56, '<p>123</p>', 8, 1526708555, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (195, 55, '<img src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=10\" />', 10, 1526708563, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (196, 55, 'test img<img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 9, 1526708595, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (197, 56, '<p>123<br/></p>', 10, 1526708624, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (198, 56, 'test img<img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 9, 1526708668, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (199, 56, '<p>关注我</p><img src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=10\" />', 10, 1526708685, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (200, 56, 'test img<script src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=9\"></script>', 9, 1526708769, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (201, 55, 'test img<script src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=9\"></script>', 9, 1526708780, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (202, 56, 'test img<script src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=9\"></script>', 9, 1526708783, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (203, 56, '<p>11111</p>', 6, 1526708792, 2130706433, 1);
INSERT INTO `bbs_reply` VALUES (204, 56, '<p>Tianye Test002</p></div><image src=http://local.premeate.songboy.net/home/index.php?m=user', 12, 1526708818, 3232237673, 1);
INSERT INTO `bbs_reply` VALUES (205, 56, '<p>hhhhhhhhhhhhhhhhhhhhhhhhh</p>', 14, 1526708822, 3232237676, 1);
INSERT INTO `bbs_reply` VALUES (206, 56, '<p>111<br/></p>', 9, 1526708831, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (207, 57, '<p>123<br/></p>', 10, 1526708854, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (208, 57, 'test img<script src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=9\"></script>', 9, 1526708857, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (209, 57, '<p>11<br/></p>', 9, 1526708863, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (210, 57, '<p>关注我</p><img src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=10\" />', 10, 1526708870, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (211, 56, '<p>11111</p>', 6, 1526708908, 2130706433, 1);
INSERT INTO `bbs_reply` VALUES (186, 55, '<p>test<br/></p>', 12, 1526707764, 3232237673, 1);
INSERT INTO `bbs_reply` VALUES (184, 56, '<p>测试一下哈</p>', 17, 1526685736, 2130706433, 1);
INSERT INTO `bbs_reply` VALUES (185, 55, '<p>dgsdgsd</p>', 14, 1526707727, 3232237676, 1);
INSERT INTO `bbs_reply` VALUES (220, 55, '<p>66<br/></p>', 13, 1526709497, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (221, 57, '<img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 8, 1526709505, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (222, 57, '<img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 8, 1526709537, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (223, 56, '<p>1</p>', 8, 1526709549, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (224, 56, '<img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 8, 1526709578, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (225, 57, '<p>1</p>', 8, 1526709597, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (226, 57, '<img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 8, 1526709633, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (227, 55, '111<img src=local.premeate.songboy.net/home/index.php?m=user', 13, 1526709663, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (228, 55, '<p>123<br/></p>', 10, 1526709726, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (229, 57, '<p>123<br/></p>', 10, 1526709782, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (230, 56, 'TianYe004</p></div><image src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=12\">', 12, 1526709798, 3232237673, 1);
INSERT INTO `bbs_reply` VALUES (231, 57, '<p>关注我</p><img src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=10\" />', 10, 1526709811, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (232, 57, '<p>关注我</p><img src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=10\" />', 10, 1526709817, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (233, 55, '123<img src = \"local.premeate.songboy.net/home/index.php?m=user', 13, 1526709817, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (234, 57, '<p>关注我</p><img src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=10\" />', 10, 1526709823, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (235, 57, '<p>关注我</p><img src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=10\" />', 10, 1526709828, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (236, 57, '<p>关注我</p><img src=\"http://local.premeate.songboy.net/home/index.php?m=user&a=follow&uid=10\" />', 10, 1526709834, 3232237675, 1);
INSERT INTO `bbs_reply` VALUES (237, 65, 'ttest img<script src=\"http://local.premeate.songboy.net/home/index.php?m=user', 9, 1526709902, 3232237678, 1);
INSERT INTO `bbs_reply` VALUES (238, 55, '<img src=\"local.premeate.songboy.net/home/index.php?m=user', 13, 1526710115, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (239, 57, '</div><img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 13, 1526710639, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (240, 67, '1<script type=\"text/javascript\" src=\"myscripts.js\"></script>', 8, 1526710648, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (241, 67, '<p>1<br/></p>', 8, 1526710664, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (242, 67, '<script src=\"http://local.premeate.songboy.net/home/index.php?m=user', 8, 1526710737, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (243, 72, '42<img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 8, 1526710782, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (244, 285, '<img src=\"local.premeate.songboy.net/home/index.php?m=user', 13, 1526711989, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (245, 285, '<img src=\"http://local.premeate.songboy.net/home/_fatie.php?bk=5', 8, 1526712015, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (246, 287, '<img src=\"http://local.premeate.songboy.net/home/_fatie.php?bk=5', 8, 1526712122, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (247, 285, '</p><img src=\"local.premeate.songboy.net/home/index.php?m=user', 13, 1526712151, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (248, 285, '<p>111<br/></p>', 13, 1526712164, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (249, 287, '123<img src=\"http://local.premeate.songboy.net/home/_fatie.php?bk=5', 8, 1526712177, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (250, 287, '<img src=\"http://local.premeate.songboy.net/home/_fatie.php?bk=5', 8, 1526712227, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (251, 285, '<p>111<br/></p><img src=\"local.premeate.songboy.net/home/index.php?m=user', 13, 1526712271, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (252, 287, '<img src=\"http://local.premeate.songboy.net/home/_fatie.php?bk=5', 8, 1526712394, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (253, 285, 'www</p><img src=\"local.premeate.songboy.net/home/index.php?m=user', 13, 1526712465, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (254, 287, '<img src=\"http://local.premeate.songboy.net/home/_fatie.php?bk=5', 8, 1526712617, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (255, 285, '<p>qq<br/></p>', 13, 1526712617, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (256, 285, '<img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 13, 1526712730, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (257, 286, 'img src=\"http://local.premeate.songboy.net/home/_fatie.php?bk=5', 8, 1526712811, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (258, 285, '</div><img src=\"http://local.premeate.songboy.net/home/index.php?m=user', 13, 1526712883, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (259, 286, '<img src=\"http://local.premeate.songboy.net/home/_fatie.php?bk=5', 8, 1526712922, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (260, 286, 'w<img src = \"http://local.premeate.songboy.net/home/index.php?m=user', 13, 1526713291, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (261, 286, '<img src=\"http://local.dvwa.songboy.net/hackable/uploads/1.js\"/>', 8, 1526713336, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (262, 286, '<img src=http://local.premeate.songboy.net/home/index.php?m=user', 13, 1526713681, 3232237679, 1);
INSERT INTO `bbs_reply` VALUES (263, 287, '<script>\r\nalert(\"My First JavaScript\");\r\n</script>', 8, 1526716737, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (264, 285, '<a href=http://local.premeate.songboy.net/home/_fatie.php?bk=5', 8, 1526716938, 3232237680, 1);
INSERT INTO `bbs_reply` VALUES (265, 289, '<p>alert(&#39;sadasd&#39;)</p>', 8, 1526789733, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (266, 289, '<p>&lt;script&gt;alert(&#39;sadasdasd&#39;)&lt;script&gt;</p>', 8, 1526789761, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (267, 289, '<p><span style=\"color: rgb(97, 97, 97); font-family: -apple-system, system-ui, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, &quot;Malgun Gothic&quot;, &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\">&lt;script&gt;alert(&#39;sadasdasd&#39;)&lt;/script&gt;</span></p>', 8, 1526789776, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (268, 289, '<p>&lt;scritp&gt;</p><p><span style=\"color: #ef596f;\">alert(&#39;asdasdasd&#39;)</span></p><p>&lt;/script&gt;<br/></p>', 8, 1526789822, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (269, 289, '', 8, 1526789883, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (270, 289, '<p>&lt;script&gt;alert(&#39;sadasdasd&#39;)&lt;/script&gt;</p><p><br/></p>', 8, 1526789930, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (271, 289, '<p><span style=\"color: rgb(97, 97, 97); font-family: -apple-system, system-ui, system-ui, &quot;Segoe UI&quot;, Roboto, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;Hiragino Kaku Gothic Pro&quot;, Meiryo, &quot;Malgun Gothic&quot;, &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\">&lt;script&gt;alert(&#39;sadasdasd&#39;)&lt;/script&gt;</span></p>', 8, 1526789967, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (272, 285, '<p>测试</p>', 6, 1527001758, 3232286701, 1);
INSERT INTO `bbs_reply` VALUES (273, 285, '<p>特殊</p>', 6, 1527001773, 3232286701, 1);
INSERT INTO `bbs_reply` VALUES (274, 285, '<p>123123123123</p>', 6, 1528006276, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (275, 285, '<p>123</p>', 6, 1528006284, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (276, 374, '<p>3123123123</p>', 6, 1528006312, 3232286655, 1);
INSERT INTO `bbs_reply` VALUES (277, 374, '<p>3234234</p>', 6, 1528006375, 3232286701, 1);
INSERT INTO `bbs_reply` VALUES (278, 768, '<p>1111</p>', 6, 1528033317, 2130706433, 1);
INSERT INTO `bbs_reply` VALUES (279, 768, '<p>cesdfsdf</p>', 6, 1528033690, 2130706433, 1);
INSERT INTO `bbs_reply` VALUES (280, 782, '<p>那就在测试一下吧</p>', 6, 1528037517, 2130706433, 1);
INSERT INTO `bbs_reply` VALUES (281, 782, '<p>现在进行尝试发帖试试</p>', 6, 1528037885, 2130706433, 1);

-- ----------------------------
-- Table structure for bbs_user
-- ----------------------------
DROP TABLE IF EXISTS `bbs_user`;
CREATE TABLE `bbs_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '72user',
  `email` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '72pass',
  `rtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `rip` bigint(11) NOT NULL DEFAULT 0,
  `admins` int(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_user
-- ----------------------------
INSERT INTO `bbs_user` VALUES (6, 'admin', '', '2c75a56335a50f86c3efcfb5a1be43ed', 1525735766, 0, 1);
INSERT INTO `bbs_user` VALUES (7, 'changqing123456', '776070848@qq.com', '21232f297a57a5a743894a0e4a801fc3', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (8, '222222', '776070848@qq.com', '21232f297a57a5a743894a0e4a801fc3', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (9, '333333', '776070848@qq.com', '21232f297a57a5a743894a0e4a801fc3', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (10, '444444', '776070848@qq.com', 'e10adc3949ba59abbe56e057f20f883e', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (11, '555555', '776070848@qq.com', '21232f297a57a5a743894a0e4a801fc3', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (12, '777777', '776070848@qq.com', '21232f297a57a5a743894a0e4a801fc3', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (13, '888888', '776070848@qq.com', 'c4ca4238a0b923820dcc509a6f75849b', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (14, '666666', '776070848@qq.com', '21232f297a57a5a743894a0e4a801fc3', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (15, '999999', '776070848@qq.com', '21232f297a57a5a743894a0e4a801fc3', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (16, '333333', '776070848@qq.com', '21232f297a57a5a743894a0e4a801fc3', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (17, '444444', '776070848@qq.com', '21232f297a57a5a743894a0e4a801fc3', 1526606850, 3232237169, 0);
INSERT INTO `bbs_user` VALUES (18, 'xx291180782', '291180782@qq.com', 'e10adc3949ba59abbe56e057f20f883e', 1527992467, 3232286655, 0);

-- ----------------------------
-- Table structure for bbs_user_detail
-- ----------------------------
DROP TABLE IF EXISTS `bbs_user_detail`;
CREATE TABLE `bbs_user_detail`  (
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `t_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '汤青松',
  `age` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `sex` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `edu` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `signed` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '../../resorec/images/userhead/default.gif',
  `telphone` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '13888888888',
  `qq` int(10) UNSIGNED NOT NULL DEFAULT 888888,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'soupqingsong@foxmail.com',
  `brithday` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `picm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '../../resorec/images/userhead/defaultm.gif',
  `pics` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '../../resorec/images/userhead/defaults.gif',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_user_detail
-- ----------------------------
INSERT INTO `bbs_user_detail` VALUES (6, '汤青松', 0, 0, 0, '23', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, 'soupqingsong@foxmail.com', 0, '/resorce/images/userhead/ad9f521aa6e50cbc13617e243e0b74b7.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (7, '汤青松', 0, 0, 0, NULL, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (8, '汤青松333', 0, 0, 0, '23', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, 'soupqingsong@foxmail.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (9, '汤青松', 0, 0, 0, NULL, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (10, '汤青松', 0, 0, 0, '555555', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (11, '汤青松', 0, 0, 0, '555555', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 860400, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (12, 'Tony', 0, 0, 0, '', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 10000, '776070848@qq.com', 270774000, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (13, '汤青松', 0, 0, 0, NULL, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (14, '汤青松', 0, 0, 0, NULL, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (15, '汤青松', 0, 0, 0, NULL, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (16, '汤青松', 0, 0, 0, NULL, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (17, '汤青松', 0, 0, 0, NULL, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');
INSERT INTO `bbs_user_detail` VALUES (18, '汤青松', 0, 0, 0, NULL, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '13888888888', 888888, '776070848@qq.com', 0, '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png', '/resorce/images/userhead/216bb6cc72448bfbf807912cc0719f4f.png');

-- ----------------------------
-- Table structure for pk_app_hadskycloudserver_cloudpay_record
-- ----------------------------
DROP TABLE IF EXISTS `pk_app_hadskycloudserver_cloudpay_record`;
CREATE TABLE `pk_app_hadskycloudserver_cloudpay_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hs_id` bigint(20) NOT NULL DEFAULT 0,
  `uid` int(11) NOT NULL DEFAULT 0,
  `rmb` int(11) NOT NULL DEFAULT 0,
  `tiandou` int(11) NOT NULL DEFAULT 0,
  `createtime` int(11) NOT NULL DEFAULT 0,
  `finishtime` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of pk_app_hadskycloudserver_cloudpay_record
-- ----------------------------

-- ----------------------------
-- Table structure for pk_app_hadskycloudserver_weixinlogin_record
-- ----------------------------
DROP TABLE IF EXISTS `pk_app_hadskycloudserver_weixinlogin_record`;
CREATE TABLE `pk_app_hadskycloudserver_weixinlogin_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0,
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `idcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `regtime` int(11) NOT NULL DEFAULT 0,
  `logtime` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_app_hadskycloudserver_weixinlogin_record
-- ----------------------------

-- ----------------------------
-- Table structure for pk_app_puyuetian_sms_record
-- ----------------------------
DROP TABLE IF EXISTS `pk_app_puyuetian_sms_record`;
CREATE TABLE `pk_app_puyuetian_sms_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pn` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date` int(8) NOT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'no',
  `msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `datetime` datetime(0) NOT NULL,
  `code` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_app_puyuetian_sms_record
-- ----------------------------

-- ----------------------------
-- Table structure for pk_download_record
-- ----------------------------
DROP TABLE IF EXISTS `pk_download_record`;
CREATE TABLE `pk_download_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `downloadid` int(11) NOT NULL DEFAULT 0,
  `uid` int(11) NOT NULL DEFAULT 0,
  `tiandou` int(11) NOT NULL DEFAULT 0,
  `datetime` bigint(20) NOT NULL DEFAULT 20000101000000,
  `leixing` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of pk_download_record
-- ----------------------------

-- ----------------------------
-- Table structure for pk_read
-- ----------------------------
DROP TABLE IF EXISTS `pk_read`;
CREATE TABLE `pk_read`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文章的id',
  `sortid` int(11) NOT NULL DEFAULT 0 COMMENT '文章所属版块id',
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发布人uid',
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章标题',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章内容',
  `looknum` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '浏览次数',
  `zannum` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '被赞次数',
  `posttime` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发布时间',
  `readlevel` int(11) NOT NULL DEFAULT 0 COMMENT '文章阅读权限',
  `replyuid` int(11) UNSIGNED NOT NULL DEFAULT 2 COMMENT '最后回复人uid',
  `replycontent` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '最新回复内容',
  `replytime` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后回复时间',
  `replyip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后回复人的ip',
  `postip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发布人的ip',
  `top` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章置顶',
  `high` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章精华',
  `locked` tinyint(3) NOT NULL DEFAULT 0,
  `replyafterlook` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '回复后可见',
  `data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文章其他数据',
  `del` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1为删除，0为正常',
  `activetime` int(11) NOT NULL DEFAULT 0 COMMENT '最后活动时间',
  `replyid` int(11) NOT NULL DEFAULT 0 COMMENT '回复的id',
  `fs` int(11) NOT NULL DEFAULT 1 COMMENT '回复的楼层数',
  `label` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '标签',
  `jvhuo_gid` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_read
-- ----------------------------
INSERT INTO `pk_read` VALUES (1, 1, 1, '欢迎您使用HadSky轻论坛！', '您好，我是乐天，HadSky轻论坛的创始人及开发者，很高兴您选择HadSky轻论坛，HadSky是一款国产、原创、开源、免费的掌上轻论坛系统，HadSky基于puyuetianPHP和puyuetianUI开发，整套软件版权都归作者蒲乐天所有，整套原创，您可以放心使用。<p><br></p><p>使用HadSky轻论坛您必须遵守HadSky用户协议<a target=\"_blank\" href=\"http://www.hadsky.com/index.php?c=read&amp;id=281&amp;page=1\">http://www.hadsky.com/index.php?c=read&amp;id=281&amp;page=1</a>，请您仔细阅读用户协议，并遵守协议，我们才能更好的为您服务。</p><p><br></p><p>购买授权成为VIP会员享受更好的待遇和服务，个人版授权77元/年，尊享版授权770元/年，买2年送1年，买5年送3年，同时拥有会员标识VIP或SVIP，赠送海量天豆和高级群待遇，购买地址：<a target=\"_blank\" href=\"http://www.hadsky.com/htmlpage-purchase.html\">http://www.hadsky.com/htmlpage-purchase.html</a></p><p><br></p><p>若您在试用过程中遇到了bug或问题，请及时反馈到我们的官坛<a target=\"_blank\" href=\"http://www.hadsky.com\">http://www.hadsky.com</a></p><p></p><p><br></p><p>感谢您选择HadSky，让我们一起努力让HadSky更好、更美、更强大！</p><p><br></p><p style=\"text-align: right;\">By puyuetian</p><p style=\"text-align: right;\">2018年07月07日</p>', 8, 0, 1469069740, 0, 2, NULL, 0, NULL, '::1', 1, 1, 0, 0, NULL, 0, 1554345863, 1, 2, '原创,爆料', 0);
INSERT INTO `pk_read` VALUES (2, 1, 1, 'HadSky云服务，免签约在线支付及短信发送功能！', 'HadSky开启了云登录与云支付功能，只要您配置好与HadSky的sitekey即可实现免签约在线支付宝支付和免签约登录第三方账号！<p>同时还具有云短信发送功能及其他更多高级功能，欢迎大家使用！<br></p><p><br></p><p>详情地址：<a target=\"_blank\" href=\"http://www.hadsky.com/app-puyuetian_documents-index.html#rid1924\">http://www.hadsky.com/app-puyuetian_documents-index.html#rid1924</a></p><p><br></p><p>HadSky发展离不开大家的支持，欢迎大家提出建议和反馈问题！</p><p></p>', 7, 0, 1500722725, 0, 2, NULL, 0, NULL, '::1', 0, 0, 0, 0, NULL, 0, 1500722725, 0, 1, '原创', 0);
INSERT INTO `pk_read` VALUES (3, 1, 1, '感谢您安装及使用HadSky轻论坛，送您一套新模板！', '感谢您安装及使用HadSky轻论坛，送您一套新模板 - FlyTemplate轻社区模板，您可以在后台 - 应用 - 本地模板处进行模板的切换。', 1, 0, 1603766768, 0, 2, NULL, 0, NULL, '127.0.0.1', 0, 0, 0, 0, NULL, 0, 1603766768, 0, 1, '', 0);
INSERT INTO `pk_read` VALUES (4, 1, 1, '<font class=\"pk-hadsky\" color=\"#FF0000\">在线发卡、发激活码就上聚货网！</font>', '聚货网免费开店，在线直接发卡、发卡密、发激活，个人站长建站首选店铺！<p><br></p><p>直达链接：<a target=\"_blank\" href=\"https://www.jvhuo.com/?from=install_hadsky\">https://www.jvhuo.com/?from=install_hadsky</a></p>', 1, 0, 1603766890, 0, 2, NULL, 0, NULL, '127.0.0.1', 0, 0, 0, 0, NULL, 0, 1603766890, 0, 1, '', 0);
INSERT INTO `pk_read` VALUES (5, 1, 1, '搜可得SEO外链网在线免费发布外链网', '搜可得SEO外链网在线免费发布外链网，提高网站曝光率，增加流量！<p><br></p><p>直达链接：<a target=\"_blank\" href=\"https://www.sokede.com/\">https://www.sokede.com/</a></p>', 2, 0, 1603767231, 0, 2, NULL, 0, NULL, '127.0.0.1', 0, 0, 0, 0, NULL, 0, 1603767231, 0, 1, '爆料', 0);
INSERT INTO `pk_read` VALUES (6, 1, 1, 'a', 'a', 1, 0, 1618052586, 0, 2, NULL, 0, NULL, '172.30.192.1', 0, 0, 0, 0, NULL, 0, 1618052586, 0, 1, '', 0);

-- ----------------------------
-- Table structure for pk_readsort
-- ----------------------------
DROP TABLE IF EXISTS `pk_readsort`;
CREATE TABLE `pk_readsort`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '版块的id',
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '所属父版块的id',
  `rank` int(11) NOT NULL DEFAULT 0 COMMENT '版块排序，从小到大',
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '版块名称',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '版块说明',
  `url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '点击分类跳转的url',
  `logourl` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '版块logo图片地址',
  `postlevel` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户发帖需要的阅读权限',
  `replylevel` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户回复需要的阅读权限',
  `adminuids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '管理员uid',
  `looklevel` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户看帖需要的阅读权限',
  `show` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1:显示 0:隐藏',
  `showchildlist` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否显示想下级版块文章',
  `label` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '标签',
  `banpostread` int(11) NOT NULL DEFAULT 0 COMMENT '该板块是否允许发帖',
  `allowgroupids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '允许进入的用户组',
  `webtitle` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `webkeywords` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `webdescription` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '版块分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_readsort
-- ----------------------------
INSERT INTO `pk_readsort` VALUES (1, 0, 0, '新版块', '', '', '', 0, 0, '', 0, 1, 0, '原创,爆料', 0, NULL, NULL, NULL, NULL);
INSERT INTO `pk_readsort` VALUES (2, 1, 0, '子版块', '', '', '', 0, 0, '', 0, 1, 0, '', 0, '', '', '', '');

-- ----------------------------
-- Table structure for pk_reply
-- ----------------------------
DROP TABLE IF EXISTS `pk_reply`;
CREATE TABLE `pk_reply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '回复的id',
  `rid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '回复的文章id',
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '回复人uid',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '回复内容',
  `posttime` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '回复时间',
  `postip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发布人的ip',
  `zannum` int(11) NOT NULL DEFAULT 0 COMMENT '赞数',
  `top` tinyint(3) NOT NULL DEFAULT 0 COMMENT '置顶',
  `del` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1删除，0正常',
  `fnum` int(11) NOT NULL DEFAULT 0 COMMENT '当前回复的楼层数',
  `data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '回复表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_reply
-- ----------------------------
INSERT INTO `pk_reply` VALUES (1, 1, 1, 'HS7beta版本发布', 1554345863, '::1', 0, 0, 0, 2, NULL);

-- ----------------------------
-- Table structure for pk_set
-- ----------------------------
DROP TABLE IF EXISTS `pk_set`;
CREATE TABLE `pk_set`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '基本设置id',
  `setname` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变量名称',
  `setvalue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '变量值',
  `noload` tinyint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `setname`(`setname`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 246 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统设置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_set
-- ----------------------------
INSERT INTO `pk_set` VALUES (2, 'bbcodemarks', '<b><i><u><strong><font><pre><code><p><span><table><thead><tbody><tfoot><tr><td><th><a><em><h1><h2><h3><h4><h5><h6><img><label><ul><ol><li><br><audio><embed><video>', 0);
INSERT INTO `pk_set` VALUES (3, 'footerhtmlcode', '<a target=\"_blank\" href=\"https://www.hadsky.com\" class=\"\" onclick=\"\">HadSky轻论坛</a>\n\n			<a href=\"https://www.jvhuo.com\" class=\"\" target=\"_blank\" onclick=\"\">聚货网|在线发卡</a><a href=\"https://www.sokede.com/\" class=\"\" target=\"_blank\" onclick=\"\">搜可得SEO外链网</a>', 0);
INSERT INTO `pk_set` VALUES (4, 'headerhtmlcode', '<a target=\"_blank\" href=\"https://www.hadsky.com\" class=\"\" onclick=\"\">官方论坛</a>', 0);
INSERT INTO `pk_set` VALUES (5, 'quotes', '支持原创软件，抵制盗版，共创美好明天！', 0);
INSERT INTO `pk_set` VALUES (6, 'templatename', 'puyuetian_fly', 0);
INSERT INTO `pk_set` VALUES (7, 'webdescription', 'HadSky轻论坛 - 轻、快、简的原创论坛系统！这是一款对非营利性个人用户免费开放的论坛系统，基于原创的puyuetianPHP快速开发框架设计，新的论坛，新的世界！', 0);
INSERT INTO `pk_set` VALUES (8, 'webkeywords', 'hadsky,有天,轻论坛,puyuetian,蒲乐天', 0);
INSERT INTO `pk_set` VALUES (9, 'weblogo', 'template/default/img/logo.png', 0);
INSERT INTO `pk_set` VALUES (10, 'webname', '有天轻论坛', 0);
INSERT INTO `pk_set` VALUES (11, 'reg', '1', 0);
INSERT INTO `pk_set` VALUES (12, 'reguserquanxian', 'bbcode,login,lookuser,postreply,uploadfile,uploadhead,search,postread,lookread,download', 0);
INSERT INTO `pk_set` VALUES (13, 'readlistnum', '10', 0);
INSERT INTO `pk_set` VALUES (14, 'replylistnum', '10', 0);
INSERT INTO `pk_set` VALUES (15, 'logotext', 'HadSky', 0);
INSERT INTO `pk_set` VALUES (16, 'qiandaojifen', '10', 0);
INSERT INTO `pk_set` VALUES (17, 'qiandaotiandou', '10', 0);
INSERT INTO `pk_set` VALUES (18, 'readsort', '', 0);
INSERT INTO `pk_set` VALUES (19, 'navhtmlcode', '<li><a href=\"index.php?c=home\">首页</a></li>\n<li><a href=\"index.php?c=list\">动态</a></li>\n<li><a href=\"index.php?c=forum\">版块</a></li>\n<li><a target=\"_blank\" href=\"http://www.hadsky.com/app-puyuetian_documents-index.html\">使用文档</a></li>\n\n<li><a target=\"_blank\" href=\"http://www.hadsky.com/htmlpage-purchase.html\">购买产品</a></li>\n<li><a target=\"_blank\" href=\"http://www.hadsky.com/read-281-1.html\">用户协议</a></li>', 0);
INSERT INTO `pk_set` VALUES (20, 'uploadfiletypes', 'jpg|jpeg|gif|bmp|png|zip|rar|txt|doc', 0);
INSERT INTO `pk_set` VALUES (21, 'uploadfilesize', '2000', 0);
INSERT INTO `pk_set` VALUES (22, 'postreadjifen', '5', 0);
INSERT INTO `pk_set` VALUES (23, 'postreadtiandou', '5', 0);
INSERT INTO `pk_set` VALUES (24, 'postreplyjifen', '2', 0);
INSERT INTO `pk_set` VALUES (25, 'postreplytiandou', '2', 0);
INSERT INTO `pk_set` VALUES (27, 'defaultpage', 'list', 0);
INSERT INTO `pk_set` VALUES (29, 'rewriteurl', '0', 0);
INSERT INTO `pk_set` VALUES (30, 'downloadfilernd', '258', 0);
INSERT INTO `pk_set` VALUES (31, 'openverifycode', '0', 0);
INSERT INTO `pk_set` VALUES (32, 'phonetemplatename', 'puyuetian_fly', 0);
INSERT INTO `pk_set` VALUES (33, 'regmessage', '恭喜您注册成功！', 0);
INSERT INTO `pk_set` VALUES (44, 'jifenname', '经验', 0);
INSERT INTO `pk_set` VALUES (45, 'tiandouname', '金钱', 0);
INSERT INTO `pk_set` VALUES (46, 'regjifen', '0', 0);
INSERT INTO `pk_set` VALUES (47, 'regtiandou', '0', 0);
INSERT INTO `pk_set` VALUES (48, 'friendlinks', '<a target=\"_blank\" href=\"https://www.hadsky.com\" class=\"\" onclick=\"\">HadSky轻论坛</a><a href=\"https://www.jvhuo.com\" class=\"\" target=\"_blank\" onclick=\"\">聚货网|在线发卡</a><a href=\"https://www.sokede.com/\" class=\"\" target=\"_blank\" onclick=\"\">搜可得SEO外链网</a>', 0);
INSERT INTO `pk_set` VALUES (49, 'postingtimeinterval', '30', 0);
INSERT INTO `pk_set` VALUES (50, 'postaudit', '0', 0);
INSERT INTO `pk_set` VALUES (51, 'newuserpostwaittime', '0', 0);
INSERT INTO `pk_set` VALUES (52, 'beianhao', '&nbsp;免备案', 0);
INSERT INTO `pk_set` VALUES (53, 'bbcodeattrs', 'class,style,href,target,src,width,height,title,alt,border,align,valign,color,size,controls,autoplay,loop,type,face,id,lang', 0);
INSERT INTO `pk_set` VALUES (54, 'readtitlemin', '1', 0);
INSERT INTO `pk_set` VALUES (55, 'readtitlemax', '1250', 0);
INSERT INTO `pk_set` VALUES (56, 'readcontentmin', '1', 0);
INSERT INTO `pk_set` VALUES (57, 'readcontentmax', '999999', 0);
INSERT INTO `pk_set` VALUES (58, 'replycontentmin', '1', 0);
INSERT INTO `pk_set` VALUES (59, 'replycontentmax', '999999', 0);
INSERT INTO `pk_set` VALUES (60, 'replyorder', '1', 0);
INSERT INTO `pk_set` VALUES (61, 'readtopnum', '3', 0);
INSERT INTO `pk_set` VALUES (62, 'webtitle', 'HadSky', 0);
INSERT INTO `pk_set` VALUES (63, 'template_default_headhtml', '', 0);
INSERT INTO `pk_set` VALUES (64, 'template_default_banner', '<div class=\"pk-padding-top-30 pk-padding-bottom-30\"><a title=\"HadSky轻论坛\" href=\"/\"><img src=\"template/default/img/logo.png\" alt=\"HadSky\"></a></div>', 0);
INSERT INTO `pk_set` VALUES (65, 'template_default_searchrighthtml', '<div class=\"pk-text-right pk-text-xs pk-text-default\">支持原创软件，共创美好明天！</div>', 0);
INSERT INTO `pk_set` VALUES (66, 'template_default_tjwzids', '', 0);
INSERT INTO `pk_set` VALUES (67, 'template_default_jhnum', '0', 0);
INSERT INTO `pk_set` VALUES (68, 'template_default_rtnum', '0', 0);
INSERT INTO `pk_set` VALUES (69, 'defaulttemplates', 'default', 0);
INSERT INTO `pk_set` VALUES (70, 'runerrordisplay', '1', 0);
INSERT INTO `pk_set` VALUES (71, 'openreg', '1', 0);
INSERT INTO `pk_set` VALUES (85, 'app_superadmin_load', '1', 0);
INSERT INTO `pk_set` VALUES (88, 'app_puyuetianeditor_load', 'embed', 0);
INSERT INTO `pk_set` VALUES (89, 'template_default_ad1htmlcode', '', 0);
INSERT INTO `pk_set` VALUES (90, 'template_default_ad2htmlcode', '', 0);
INSERT INTO `pk_set` VALUES (91, 'template_default_ad3htmlcode', '', 0);
INSERT INTO `pk_set` VALUES (92, 'guestdata', '{\"username\":\"guest\",\"nickname\":\"游客\",\"quanxian\":\"bbcode,lookuser,postreply,search,lookread,download,nopostingtimeinterval\",\"readlevel\":\"0\",\"data\":\"{\\\"htmlcodemarks\\\":\\\"\\\",\\\"htmlcodeattrs\\\":\\\"\\\",\\\"uploadsize\\\":\\\"\\\"}\"}', 0);
INSERT INTO `pk_set` VALUES (93, 'template_default_randheadcount', '24', 0);
INSERT INTO `pk_set` VALUES (95, 'novicetraineetime', '2', 0);
INSERT INTO `pk_set` VALUES (96, 'postreadcheck', '0', 0);
INSERT INTO `pk_set` VALUES (97, 'postreplycheck', '0', 0);
INSERT INTO `pk_set` VALUES (107, 'app_puyuetian_search_load', '1', 0);
INSERT INTO `pk_set` VALUES (108, 'app_puyuetian_search_showcount', '20', 0);
INSERT INTO `pk_set` VALUES (109, 'readlistorder', 'posttime', 0);
INSERT INTO `pk_set` VALUES (110, 'showmessagecount', '50', 0);
INSERT INTO `pk_set` VALUES (111, 'closeregtip', '本站暂未开启注册功能！', 0);
INSERT INTO `pk_set` VALUES (112, 'uploadheadsize', '500', 0);
INSERT INTO `pk_set` VALUES (113, 'trylogincount', '10', 0);
INSERT INTO `pk_set` VALUES (114, 'changeuserinfoverify', '0', 0);
INSERT INTO `pk_set` VALUES (115, 'app_puyuetianeditor_pceditconfig', 'var PytConfig = \'Html,Bold,Italic,Underline,Strikethrough,Removemarks,Fontname,Fontsize,Forecolor,Backcolor,Justifyleft,Justifycenter,Justifyright,Link,Unlink,Table,Emotion,Image,File,Video,Music,Myfiles,Code,Replylook,Undo,Redo\';', 0);
INSERT INTO `pk_set` VALUES (116, 'app_puyuetianeditor_pcreadconfig', 'var PytConfig = \'Html,Bold,Italic,Underline,Emotion,Image,Code\';', 0);
INSERT INTO `pk_set` VALUES (117, 'app_puyuetianeditor_phoneeditconfig', 'var PytConfig = \'Emotion,Image,File,Link,Code,Replylook\';', 0);
INSERT INTO `pk_set` VALUES (118, 'app_puyuetianeditor_phonereadconfig', 'var PytConfig = \'Emotion,Image,Code\';', 0);
INSERT INTO `pk_set` VALUES (119, 'yunserver', '1', 0);
INSERT INTO `pk_set` VALUES (120, 'phonedomains', '', 0);
INSERT INTO `pk_set` VALUES (121, 'ifpccomephonego', '1', 0);
INSERT INTO `pk_set` VALUES (122, 'phonedefaulttemplates', 'default', 0);
INSERT INTO `pk_set` VALUES (123, 'readlistshowbks', '', 0);
INSERT INTO `pk_set` VALUES (124, 'readlisthiddenbks', '', 0);
INSERT INTO `pk_set` VALUES (125, 'usernameeverychars', '1', 0);
INSERT INTO `pk_set` VALUES (126, 'regway', 'normal', 0);
INSERT INTO `pk_set` VALUES (127, 'regreadlevel', '10', 0);
INSERT INTO `pk_set` VALUES (128, 'reguserdata', '', 0);
INSERT INTO `pk_set` VALUES (129, 'app_puyuetianeditor_quickpost', '1', 0);
INSERT INTO `pk_set` VALUES (130, 'app_puyuetianeditor_quickeditconfig', 'var PytConfig = \'Html,Bold,Italic,Underline,Strikethrough,Justifyleft,Justifycenter,Justifyright,Link,Unlink,Emotion,Image,Code,Replylook,Undo,Redo\';', 0);
INSERT INTO `pk_set` VALUES (131, 'chkcsrfval', 'b02e0dae59241656173c568fb32d0287', 0);
INSERT INTO `pk_set` VALUES (132, 'uploadfilecount', '5', 0);
INSERT INTO `pk_set` VALUES (133, 'dayuploadfilesize', '10000', 0);
INSERT INTO `pk_set` VALUES (134, 'downloadedrecord', '1', 0);
INSERT INTO `pk_set` VALUES (147, 'key', '00Pr3CHz7wQWQGcWQBwgri6GaxHlkSKO', 0);
INSERT INTO `pk_set` VALUES (148, 'chkcsrf', '0', 0);
INSERT INTO `pk_set` VALUES (149, 'chkcsrfpages', '', 0);
INSERT INTO `pk_set` VALUES (150, 'sitestatus', '0', 0);
INSERT INTO `pk_set` VALUES (151, 'siteclosedtip', '维护中...', 0);
INSERT INTO `pk_set` VALUES (152, 'usercookieslife', '31536000', 0);
INSERT INTO `pk_set` VALUES (153, 'userloginemailtip', '0', 0);
INSERT INTO `pk_set` VALUES (154, 'regusergroupid', '1', 0);
INSERT INTO `pk_set` VALUES (155, 'app_verifycode_load', '1', 0);
INSERT INTO `pk_set` VALUES (156, 'app_verifycode_page', '', 0);
INSERT INTO `pk_set` VALUES (157, 'app_verifycode_chars', '1234567890', 0);
INSERT INTO `pk_set` VALUES (158, 'app_verifycode_length', '', 0);
INSERT INTO `pk_set` VALUES (159, 'app_verifycode_width', '', 0);
INSERT INTO `pk_set` VALUES (160, 'app_verifycode_height', '', 0);
INSERT INTO `pk_set` VALUES (161, 'app_verifycode_fontsize', '', 0);
INSERT INTO `pk_set` VALUES (162, 'app_puyuetianeditor_watermark_text', '', 0);
INSERT INTO `pk_set` VALUES (163, 'app_puyuetianeditor_watermark_textcolor_r', '255', 0);
INSERT INTO `pk_set` VALUES (164, 'app_puyuetianeditor_watermark_textcolor_g', '255', 0);
INSERT INTO `pk_set` VALUES (165, 'app_puyuetianeditor_watermark_textcolor_b', '255', 0);
INSERT INTO `pk_set` VALUES (166, 'app_puyuetianeditor_watermark_pp', '0', 0);
INSERT INTO `pk_set` VALUES (167, 'app_puyuetianeditor_watermark_px', '5', 0);
INSERT INTO `pk_set` VALUES (168, 'app_puyuetianeditor_watermark_py', '24', 0);
INSERT INTO `pk_set` VALUES (169, 'app_puyuetianeditor_watermark_fontsize', '14', 0);
INSERT INTO `pk_set` VALUES (170, 'defaultlabel', '原创,爆料', 0);
INSERT INTO `pk_set` VALUES (171, 'app_hadskycloudserver_load', 'embed', 0);
INSERT INTO `pk_set` VALUES (172, 'app_hadskycloudserver_sitekey', '', 0);
INSERT INTO `pk_set` VALUES (173, 'app_hadskycloudserver_qqlogin_openreg', '0', 0);
INSERT INTO `pk_set` VALUES (174, 'app_hadskycloudserver_weibologin_openreg', '0', 0);
INSERT INTO `pk_set` VALUES (175, 'app_hadskycloudserver_baidulogin_openreg', '0', 0);
INSERT INTO `pk_set` VALUES (176, 'app_hadskycloudserver_tiandouduihuanshu', '10', 0);
INSERT INTO `pk_set` VALUES (177, 'phonedefaultpage', 'list', 0);
INSERT INTO `pk_set` VALUES (178, 'mysqlcachecycle', '0', 0);
INSERT INTO `pk_set` VALUES (179, 'mysqlcachetables', 'read', 0);
INSERT INTO `pk_set` VALUES (180, 'safe_request_uri', '0', 0);
INSERT INTO `pk_set` VALUES (181, '_webos', 'HadSky', 0);
INSERT INTO `pk_set` VALUES (182, 'oldusercentertonewusercenter', '1', 0);
INSERT INTO `pk_set` VALUES (183, 'postmessagemaxnum', '1000', 0);
INSERT INTO `pk_set` VALUES (184, 'postreadtitlecolorusergroup', '', 0);
INSERT INTO `pk_set` VALUES (185, 'banpostwords', '', 0);
INSERT INTO `pk_set` VALUES (186, 'regfriends', '', 0);
INSERT INTO `pk_set` VALUES (187, 'regagreement', '请在后台 - 全局 - 注册相关 - 用户注册协议处设置内容', 0);
INSERT INTO `pk_set` VALUES (188, 'banregwords', '', 0);
INSERT INTO `pk_set` VALUES (189, 'webaddedwords', 'HadSky', 0);
INSERT INTO `pk_set` VALUES (190, 'mustrewriteurl', '1', 0);
INSERT INTO `pk_set` VALUES (191, 'cookie_domain', '', 0);
INSERT INTO `pk_set` VALUES (192, 'usermultipleonline', '1', 0);
INSERT INTO `pk_set` VALUES (193, 'supermanloginemailtip', '1', 0);
INSERT INTO `pk_set` VALUES (194, 'app_verifycode_opensliding', '0', 0);
INSERT INTO `pk_set` VALUES (195, 'app_puyuetianeditor_pcheight', '500px', 0);
INSERT INTO `pk_set` VALUES (196, 'app_puyuetianeditor_phoneheight', '200px', 0);
INSERT INTO `pk_set` VALUES (197, 'app_puyuetianeditor_compress_wh', '1366,0', 0);
INSERT INTO `pk_set` VALUES (198, 'app_puyuetianeditor_showfile_ad1', '', 0);
INSERT INTO `pk_set` VALUES (199, 'app_puyuetianeditor_showfile_ad2', '', 0);
INSERT INTO `pk_set` VALUES (200, 'app_hadskycloudserver_nodes', '{\"shanghai\":\"\\u534e\\u4e1c\\uff08\\u4e0a\\u6d77\\uff09\",\"guangzhou\":\"\\u534e\\u5357\\uff08\\u5e7f\\u5dde\\uff09\",\"hk\":\"\\u9999\\u6e2f\"}', 0);
INSERT INTO `pk_set` VALUES (201, 'app_hadskycloudserver_node', 'auto', 0);
INSERT INTO `pk_set` VALUES (202, 'app_hadskycloudserver_weixinlogin_openreg', '0', 0);
INSERT INTO `pk_set` VALUES (203, 'app_hadskycloudserver_sms_open', '1', 0);
INSERT INTO `pk_set` VALUES (204, 'app_puyuetian_sms_verifycode', '1', 0);
INSERT INTO `pk_set` VALUES (205, 'app_puyuetian_sms_mustreg', '0', 0);
INSERT INTO `pk_set` VALUES (206, 'app_puyuetian_sms_pnmax', '', 0);
INSERT INTO `pk_set` VALUES (207, 'app_puyuetian_sms_ipmax', '', 0);
INSERT INTO `pk_set` VALUES (208, 'app_filesmanager_load', '1', 0);
INSERT INTO `pk_set` VALUES (209, 'app_jvhuo_load', '1', 0);
INSERT INTO `pk_set` VALUES (210, 'app_jvhuo_apiuid', '', 0);
INSERT INTO `pk_set` VALUES (211, 'app_jvhuo_apicode', '', 0);
INSERT INTO `pk_set` VALUES (212, 'app_jvhuo_postuids', '', 0);
INSERT INTO `pk_set` VALUES (213, 'app_jvhuo_postbkid', '', 0);
INSERT INTO `pk_set` VALUES (214, 'app_jvhuo_postlabel', '', 0);
INSERT INTO `pk_set` VALUES (215, 'app_mysqlmanager_load', '1', 0);
INSERT INTO `pk_set` VALUES (216, 'securelogin', '1', 0);
INSERT INTO `pk_set` VALUES (217, 'template_puyuetian_fly_hotreadsortids', '', 0);
INSERT INTO `pk_set` VALUES (218, 'template_puyuetian_fly_hotreadcount', '10', 0);
INSERT INTO `pk_set` VALUES (219, 'template_puyuetian_fly_hotreaddays', '', 0);
INSERT INTO `pk_set` VALUES (220, 'template_puyuetian_fly_ad_yc1', '<div class=\"fly-panel\">\n	<h3 class=\"fly-panel-title\">后台 - 应用 - 本地模板 - FlyTemplate设置 - 右侧广告位1</h3>\n	<ul class=\"fly-panel-main fly-list-static\">\n		<li><a target=\"_blank\" href=\"https://www.hadsky.com/app-puyuetian_documents-index.html\">HadSky使用文档</a></li>\n		<li><a target=\"_blank\" href=\"https://www.hadsky.com/read-4673-1.html\">HS建站攻略，花最少的钱，建最“骚”的站！</a></li>\n		<li><a target=\"_blank\" href=\"https://www.hadsky.com/app-hadskydownload-app.html\">官方APP体验</a></li>\n		<li><a target=\"_blank\" href=\"https://www.hadsky.com/read-6221-1.html\">HS仿抖音插件：HS轻视频</a></li>\n		<li><a target=\"_blank\" href=\"https://www.hadsky.com/appstore.html?auto=1&dir=puyuetian_copyweibo&rid=4645\">仿微博手机模板</a></li>\n	</ul>\n</div>', 0);
INSERT INTO `pk_set` VALUES (221, 'template_puyuetian_fly_ad_yc2', '<div class=\"fly-panel\">\n	<div class=\"fly-panel-main\">\n		<a href=\"#\"><img src=\"template/puyuetian_fly/res/images/350ad.png\" alt=\"ad\"></a>\n	</div>\n</div>', 0);
INSERT INTO `pk_set` VALUES (222, 'template_puyuetian_fly_ad_yc3', '<div class=\"fly-panel\">\n	<div class=\"fly-panel-title\">后台 - 应用 - 本地模板 - FlyTemplate设置 - 右侧广告位3</div>\n	<div class=\"fly-panel-main\">\n		<a target=\"_blank\" href=\"https://www.jvhuo.com/?from=install_hadsky\">聚货网 - 免费在线开店发卡</a>\n	</div>\n</div>', 0);
INSERT INTO `pk_set` VALUES (223, 'template_puyuetian_fly_ad_yc4', '<div class=\"fly-panel\">\n	<a href=\"#\"><img src=\"template/puyuetian_fly/res/images/380ad.png\" alt=\"ad\"></a>\n</div>', 0);
INSERT INTO `pk_set` VALUES (224, 'template_puyuetian_fly_listshowvideo', '1', 0);
INSERT INTO `pk_set` VALUES (225, 'template_puyuetian_fly_readimgnum', '3', 0);
INSERT INTO `pk_set` VALUES (226, 'admin_login_notice_phone', '1', 0);
INSERT INTO `pk_set` VALUES (227, 'app_puyuetian_sms_signname', '', 0);
INSERT INTO `pk_set` VALUES (228, 'globalcdn', '0', 0);
INSERT INTO `pk_set` VALUES (229, 'app_systememail_load', '0', 0);
INSERT INTO `pk_set` VALUES (230, 'app_systememail_phpmailer', '0', 0);
INSERT INTO `pk_set` VALUES (231, 'app_systememail_debug', '0', 0);
INSERT INTO `pk_set` VALUES (232, 'app_systememail_timeoutseconds', '', 0);
INSERT INTO `pk_set` VALUES (233, 'app_systememail_smtp', '', 0);
INSERT INTO `pk_set` VALUES (234, 'app_systememail_port', '', 0);
INSERT INTO `pk_set` VALUES (235, 'app_systememail_user', '', 0);
INSERT INTO `pk_set` VALUES (236, 'app_systememail_pass', '', 0);
INSERT INTO `pk_set` VALUES (237, 'app_systememail_emailverify', '0', 0);
INSERT INTO `pk_set` VALUES (238, 'app_systememail_regsendemail', '1', 0);
INSERT INTO `pk_set` VALUES (239, 'app_systememail_replysendemail', '1', 0);
INSERT INTO `pk_set` VALUES (240, 'app_puyuetianeditor_pceasypostread', '0', 0);
INSERT INTO `pk_set` VALUES (241, 'app_puyuetianeditor_pceasypostreply', '0', 0);
INSERT INTO `pk_set` VALUES (242, 'app_puyuetianeditor_phoneeasypostread', '1', 0);
INSERT INTO `pk_set` VALUES (243, 'app_puyuetianeditor_phoneeasypostreply', '1', 0);
INSERT INTO `pk_set` VALUES (244, 'app_puyuetianeditor_editconfig', '', 0);
INSERT INTO `pk_set` VALUES (245, 'app_puyuetianeditor_readconfig', '', 0);

-- ----------------------------
-- Table structure for pk_upload
-- ----------------------------
DROP TABLE IF EXISTS `pk_upload`;
CREATE TABLE `pk_upload`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上传用户',
  `rid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '所在的帖子id',
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '上传的文件名',
  `suffix` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文件后缀',
  `uploadtime` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上传时间',
  `datetime` bigint(20) NOT NULL DEFAULT 0 COMMENT '上传时间YYYYMMDDHHIISS',
  `rand` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产生的随机数',
  `idcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '识别码',
  `jifen` int(11) NOT NULL DEFAULT 0 COMMENT '消耗积分',
  `tiandou` int(11) NOT NULL DEFAULT 0 COMMENT '消耗的天豆',
  `target` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '所存在的文件夹',
  `downloadcount` int(11) NOT NULL DEFAULT 0 COMMENT '下载次数',
  `downloadeduids` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '已下载用户的uid',
  `url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '论坛附件上传记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_upload
-- ----------------------------

-- ----------------------------
-- Table structure for pk_user
-- ----------------------------
DROP TABLE IF EXISTS `pk_user`;
CREATE TABLE `pk_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户的uid',
  `username` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名，仅允许字母和数字和下划线',
  `password` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '加密后的密码',
  `nickname` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户昵称',
  `quanxian` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户具有的权限',
  `tiandou` int(11) NOT NULL DEFAULT 0 COMMENT '天豆数',
  `jifen` int(11) NOT NULL DEFAULT 0 COMMENT '积分数',
  `logininfo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '最后一次登录信息',
  `reginfo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户注册信息',
  `sex` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '性别',
  `birthday` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '生日',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'email地址',
  `qq` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'QQ',
  `phone` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '手机号',
  `adress` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '联系地址',
  `sign` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户个性签名',
  `friends` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户好友',
  `readlevel` int(11) NOT NULL DEFAULT 0 COMMENT '用户阅读权限',
  `qqopenid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'qq登录openid',
  `data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户数据',
  `logintime` int(11) NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `regtime` int(11) NOT NULL DEFAULT 0 COMMENT '注册时间',
  `session_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录时产生的识别码',
  `groupid` int(11) NOT NULL DEFAULT 0 COMMENT '用户组id',
  `wxopenid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信登录openid',
  `weibo_uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `baidu_userid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `regip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `loginip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `weixin_openids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `idol` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `fans` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `collect` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_user
-- ----------------------------
INSERT INTO `pk_user` VALUES (1, 'admin', '27da85f99b6e1207a71b6c4dd6404ddb', '创始人', 'bbcode,login,lookuser,postreply,uploadfile,delread,editread,nopostreadcheck,noverifycode,admin,htmlcode,superman,nopostingtimeinterval,nopostreplycheck,editreply,delreply,uploadhead,search,postread,lookread,download', 32, 32, NULL, NULL, 's', 20180709, 'admin@hadsky.com', '', '', '', '有些梦虽然遥不可及，但并不是不可能实现。', NULL, 0, NULL, '{\"trylogindata\":\"{\\\"172.30.192.1\\\":0,\\\"172.17.0.1\\\":10}\",\"logindate\":\"20210410\",\"lasttimereadposttime\":\"1618052586\",\"privacysettings\":\"0\",\"lasttimereplyposttime\":\"1554345863\"}', 1618051280, 0, 'Sd0q27n', 0, NULL, NULL, NULL, NULL, '172.30.192.1', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for pk_user_message
-- ----------------------------
DROP TABLE IF EXISTS `pk_user_message`;
CREATE TABLE `pk_user_message`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '消息的id',
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '消息用户的uid',
  `fid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '来自哪个用户的uid',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '消息内容',
  `islook` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '此消息是否被查看',
  `addtime` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '消息添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_user_message
-- ----------------------------

-- ----------------------------
-- Table structure for pk_usergroup
-- ----------------------------
DROP TABLE IF EXISTS `pk_usergroup`;
CREATE TABLE `pk_usergroup`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usergroupname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户组名称',
  `quanxian` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户组权限',
  `readlevel` int(11) NOT NULL DEFAULT 0 COMMENT '阅读权限',
  `data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '其他数据',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pk_usergroup
-- ----------------------------
INSERT INTO `pk_usergroup` VALUES (1, '普通会员', 'bbcode,login,lookuser,postreply,uploadfile,uploadhead,search,postread,lookread,download', 10, '{\"htmlcodemarks\":\"\",\"htmlcodeattrs\":\"\",\"signcodemarks\":\"\",\"signcodeattrs\":\"\",\"uploadsize\":\"\",\"dayuploadfilesize\":\"\"}');

SET FOREIGN_KEY_CHECKS = 1;
