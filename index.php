<?php

$list = scandir(__DIR__ . '/result/');

array_shift($list);
array_shift($list);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- 可选的 Bootstrap 主题文件（一般不用引入） -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <meta charset="UTF-8">

    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div class="container">
    <div style="height: 150px"></div>
    <h2>漏洞扫描结果列表</h2>
    <a href="add.php" class="btn btn-success">添加扫描</a>
    <table class="table table-striped">
        <tr>
            <th> 序号
            </th>
            <th> URL
            </th>
            <th>创建时间
            </th>
        </tr>
        <?php foreach ($list as $key => $value) {
            $fileName = explode('.', $value)[0]; ?>
            <tr>
                <td><?= $key + 1 ?>
                </td>
                <td>
                    <a href="./result/<?= $value ?>"><?= $value ?></a>
                </td>
                <td><?= date('Y-m-d H:i:s', $fileName) ?>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>
</body>
</html>